from django import forms
from A01_2021_broken_access_control.models import BankAccount
from django.contrib.auth.models import User
from django.db.utils import OperationalError

try:
    accounts = BankAccount.objects.all()
    CHOICES = [(acc.id, User.objects.get(id=acc.user_id)) for acc in accounts]
except OperationalError:
    CHOICES = [("",)]


class TransferForm(forms.Form):
    source_account = forms.ChoiceField(choices=CHOICES)
    destination_account = forms.ChoiceField(choices=CHOICES)
    amount = forms.DecimalField(max_digits=10, decimal_places=2)
