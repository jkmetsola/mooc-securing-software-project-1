from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .forms import TransferForm
from .models import BankAccount as Account


def _obtain_account(number):
    return Account.objects.get(number=number)


@login_required
def transfer(request):
    if request.method != "POST":
        return
    form = TransferForm(request.POST)
    if form.is_valid():
        s_acc = Account.objects.get(form.cleaned_data["source_account"])
        d_acc = Account.objects.get(form.cleaned_data["destination_account"])
        amount = form.cleaned_data["amount"]
        # Perform the transfer, ensuring the source account has enough funds
        if s_acc.balance >= amount:
            s_acc.balance -= amount
            d_acc.balance += amount
            s_acc.save()
            d_acc.save()

    return render(request, "A01/transfer.html", {"form": form})


@login_required
def home(request):
    bank_account = BankAccount.objects.get(user=request.user)
    # Render the account balance in the template
    return render(request, "A01/balances.html", {"bank_account": bank_account})
