from django.apps import AppConfig


class A012021BrokenAccessControlConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "A01_2021_broken_access_control"
