import django

django.setup()
from django.contrib.auth.models import User
from A01_2021_broken_access_control.models import BankAccount


def create_users_with_bank_accounts():
    user1 = User.objects.create_user(username="user1", password="password1")
    user2 = User.objects.create_user(username="user2", password="password2")

    BankAccount.objects.create(
        user=user1, account_number="1234567890", balance=1000.00
    )
    BankAccount.objects.create(
        user=user2, account_number="9876543210", balance=500.00
    )

    print("Users with bank accounts created successfully.")


if __name__ == "__main__":
    create_users_with_bank_accounts()
