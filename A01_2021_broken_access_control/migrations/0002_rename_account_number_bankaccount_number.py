# Generated by Django 4.2.1 on 2023-05-21 17:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("A01_2021_broken_access_control", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="bankaccount",
            old_name="account_number",
            new_name="number",
        ),
    ]
