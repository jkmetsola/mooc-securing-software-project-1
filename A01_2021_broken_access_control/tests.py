from django.test import TestCase
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from .models import BankAccount


class BankAccountTestCase(TestCase):
    def setUp(self):
        # Create two users with strong, hard-to-guess passwords
        self.user1 = User.objects.create_user(
            username="user1", password="0neH@rdT0Gu3$$P@ssw0rd"
        )
        self.user2 = User.objects.create_user(
            username="user2", password="@n0therH@rdT0Gu3$$P@ssw0rd"
        )

        # Create bank accounts for these users
        BankAccount.objects.create(
            user=self.user1, account_number="12345678", balance=1000.00
        )
        BankAccount.objects.create(
            user=self.user2, account_number="87654321", balance=2000.00
        )
