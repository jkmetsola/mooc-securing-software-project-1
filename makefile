PYTHON ?= python3

all: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"}  \
		/^[^[:blank:]]+:.*##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } \
		/^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

clean: clean-venv  ## Clean workspace

##@ Chrome

chrome-versions: ## Get Google chrome version and plugin version
	@google-chrome --version
	@chromedriver --version

chrome-plugin:	## Install chrome plugin
	sudo sh install_chrome_plugin.sh

test-plugin:  ## Test chrome plugin
	venv/bin/python sanity.py

##@ TMC

tmc-help:  ## TMC help
	@echo "Course site: https://cybersecuritybase.mooc.fi/installation-guide"
	@echo "tmc test <filename>"
	@echo "tmc submit <filename>"
	@echo "tmc help"

##@ Virtual environment
venv: venv/bin/activate  ## Create virtual environment

.PHONY: clean-venv show-venv devel prod
clean-venv:  ## Remove virtual environment
	[ ! -d venv ] || rm -rf venv

show-venv: venv  ## Show virtual environment
	@venv/bin/python -c "import sys; print('Python ' + sys.version.replace('\n',''))"
	@venv/bin/pip --version
	@venv/bin/pip list

devel: venv/bin/pip-sync ## Setup python environment for development
	venv/bin/pip-sync requirements.txt $(wildcard requirements-*.txt)

prod: venv/bin/pip-sync ## Setup python environment for production
	venv/bin/pip-sync requirements.txt

venv/bin/activate:
	$(PYTHON) -m venv venv
	cp pip.conf venv/
	venv/bin/python -m pip install --upgrade pip setuptools setuptools_scm wheel
	touch venv/bin/activate

venv/bin/pip-compile: venv
	venv/bin/pip install --upgrade pip-tools
	touch venv/bin/pip-compile

venv/bin/pip-sync: venv
	venv/bin/pip install --upgrade pip-tools
	touch venv/bin/pip-sync


##@ Requirements
.PHONY: requirements
requirements: requirements.txt $(wildcard requirements-*.txt)  ## Update pinned package versions in requirements*.txt files

%.txt: %.in venv/bin/pip-compile
	venv/bin/pip-compile --annotation-style=line --resolver backtracking --verbose --strip-extras --upgrade requirements.in
	venv/bin/pip-compile --annotation-style=line --resolver backtracking --verbose --strip-extras --upgrade requirements-test.in


##@ Static analysis
check: black flake8 pylint robotidy robocop  ## Static analysis for python and robot libraries

venv/bin/%: venv/bin/activate
	venv/bin/pip install --upgrade $*
	touch venv/bin/$*

.PHONY: flake8 pylint robocop
flake8: venv/bin/flake8  ## Static analysis by flake8
	venv/bin/flake8 libraries
	venv/bin/flake8 variables

pylint: venv/bin/pylint devel  ## Static analysis by pylint
	venv/bin/pylint libraries
	venv/bin/pylint variables

black: venv/bin/black  ## Python source code formatting check
	venv/bin/black --line-length=120 --check libraries variables

format: venv/bin/black venv/bin/robotidy  ## Autoformat Python and Robot source files
	venv/bin/black --line-length=120 libraries variables
