import django

django.setup()
from django.contrib.auth.models import User
from A01_2021_broken_access_control.models import BankAccount as Account


def create_users_with_bank_accounts():
    user1 = User.objects.create_user(username="user1", password="password1")
    user2 = User.objects.create_user(username="user2", password="password2")

    Account.objects.create(user=user1, number="1234567890", balance=1000.00)
    Account.objects.create(user=user2, number="9876543210", balance=500.00)

    print("Users with bank accounts created successfully.")


if __name__ == "__main__":
    create_users_with_bank_accounts()
