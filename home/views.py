from django.shortcuts import render
from django.urls import get_resolver


def home(request):
    urlconf = get_resolver(None).url_patterns
    root_url = request.build_absolute_uri("/")
    urlconf = [f"{root_url}{url.pattern}" for url in urlconf]
    return render(request, "home.html", {"urls": urlconf})
